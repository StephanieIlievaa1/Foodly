![Screenshot (31)](https://user-images.githubusercontent.com/72206861/146407193-f38cad3f-05d2-4113-a799-b23533bfef78.png)
## FOODLY - Simple web app build with ReactJS, HTML and SASS.

## Live demo: https://stoic-albattani-1b8e57.netlify.app/

## How to run
- Navigate to frontend folder
```js
cd frontend
```
- Install all needed packages
```js
npm install
```
- And run the application
```js
npm start
```
- The app will run on http://localhost:3000/
